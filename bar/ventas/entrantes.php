<?php 

require("includes/conexion.php");

if(isset($_GET['accion']) && $_GET['accion']=="anyadir"){ 

    $id=intval($_GET['id']); 

    if(isset($_SESSION['carrito'][$id])){ 
        $_SESSION['carrito'][$id]['cantidad']++; 
    }else{ 
        $sql_s="SELECT * FROM tipocomida WHERE idcomida=$id"; 
        $query_s=mysqli_query($conexion, $sql_s); 

        if(mysqli_num_rows($query_s)!=0){ 
            $fila_s=mysqli_fetch_array($query_s); 

            $_SESSION['carrito'][$fila_s['idcomida']]=array( 
                    "cantidad" => 0, 
                    "precio" => $fila_s['precio']);
        }
    }
} 
  
?> 

<!DOCTYPE html> 

<html lang="es">
<meta charset="UTF-8">  

<head> 
    <link rel="stylesheet" href="css/estilos.css" /> 
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Pedido</title> 
</head> 
<body>
    
    <div class="fondo">
	
<div class="medio1">
    <a href="index.php?pagina=carrito"><button class="w3-button w3-deep-orange">Ir al carrito</button></a>
    <a href="index.php"><button class="w3-button w3-deep-orange">Ir a ENTRADA PEDIDO</button></a>
    </div>
<div class="centrar"> 
    
<table  class="tablecenter"> 
    
    
    <h1>ENTRANTES</h1>
    <tr> 
        <th>Id Comida</th> 
	<th>Tipo Comida</th> 
        <th>Descripción</th> 
        <th>Precio</th> 
        <th>Acción</th> 
    </tr> 

    <?php 

        $sql="SELECT * FROM tipocomida WHERE tipo='Entrantes' ORDER BY idcomida ASC"; 

        $query=mysqli_query($conexion, $sql); 

        while ($fila=mysqli_fetch_array($query)) { 
    ?> 

        <tr> 
            <td><?php echo $fila['idcomida'] ?></td> 
	    
	    <td><?php echo $fila['tipo'] ?></td>

            <td><?php echo $fila['descripcion'] ?></td> 

            <td class="numero"><?php echo $fila['precio'] ?> €</td> 

            <td><a href="index.php?pagina=comida&accion=anyadir&id=<?php echo $fila['idcomida'] ?>">Añadir al pedido</a></td> 

        </tr> 

    <?php } ?> 
	
</table>
    </div>
    </div>
</body> 
</html>