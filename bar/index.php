<!DOCTYPE HTML>

<html>
    <head>
        <title>BAR | RESTAURANTE</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
    </head>
    <body>

        <!-- Header -->
        <header id="header" class="alt">
            <div class="logo"><a href="index.html">BAR <span>RESTAURANTE</span></a></div>
            <a href="#menu">Ménu</a>
        </header>

        <!-- Nav -->
        <nav id="menu">
            <ul class="links">
                <li><a href="index.php">HOME</a></li>
                <li><a href="clientes/login.php">ZONA CLIENTES</a></li>
                <li><a href="empleados/login.php">ZONA EMPLEADOS</a></li>
                <li><a href="privada/login.php">ZONA PRIVADA</a></li>
            </ul>
        </nav>

        <!-- Banner -->
        <section id="banner">
            <div class="inner">
                <header>
                    <h1>BAR RESTAURANTE</h1>
                    <p>Nuestra pasión es cocinar con productos frescos y maridar cada plato  <br />con excelentes vinos de pequeñas bodegas con identidad propia.</p>
                </header>
                <a href="#main" class="button big scrolly">Sobre nosotros</a>
            </div>
        </section>

        <!-- Main -->
        <div id="main">

            <!-- Section -->
            <section class="wrapper style1">
                <div class="inner">
                    <!-- 2 Columns -->
                    <div class="flex flex-2">
                        <div class="col col1">
                            <div class="image round fit">
                                <a href="generic.html" class="link"><img src="assets/images/pic01.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col col2">
                            <h3>Bar Restaurante</h3>
                            <p>Bar Restaurante es un gastrobar acogedor con personalidad propia que ofrece una propuesta gastronómica tradicional con toques creativos en un ambiente dinámico y cosmopolita.
                                Por arte de magia, su cocina de mercado con recetas originales envolverá tu paladar con sus deliciosas tapas y sus platos cotidianos que encierran sorpresas.</p>
                            <p>El local se asienta en la planta baja de un edificio Modernista del siglo XIX, en pleno centro de Valencia entre la Plaza de la Reina, el Palacio Marqués de dos aguas y la Plaza del Ayuntamiento.
                                Está decorado con ilustraciones originales diseñadas por Calpurnio, y cuenta con cuatro espacios diferentes para deleite de sus visitantes: una barra larga, un comedor, una terraza abierta todo el año y un sótano secreto que dispone de una sala privada.
                                Es ideal para desayunar, almorzar, tomar el aperitivo, comer, merendar, cenar o ir de copas. . </p>
                            <a href="#" class="button">Ver más</a>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Section -->
            <section class="wrapper style2" id="compra">
                <div class="inner">
                    <div class="flex flex-2">
                        <div class="col col2">
                            <h3> justo</h3>
                                <p>Etiam posuere hendrerit arcu, ac blandit nulla. Sed congue malesuada nibh, a varius odio vehicula aliquet. Aliquam consequat, nunc quis sollicitudin aliquet, enim magna cursus auctor lacinia nunc ex blandit augue. Ut vitae neque fermentum, luctus elit fermentum, porta augue. Nullam ultricies,turpis at fermentum iaculis, nunc justo dictum dui, non aliquet erat nibh non ex.</p>
                                <p>Sed congue malesuada nibh, a varius odio vehicula aliquet. Aliquam consequat, nunc quis sollicitudin aliquet, enim magna cursus auctor lacinia nunc ex blandit augue. Ut vitae neque fermentum, luctus elit fermentum, porta augue. Nullam ultricies, turpis at fermentum iaculis, nunc justo dictum dui, non aliquet erat nibh non ex. </p>
                            <a href="login/clientes/logout.php" class="button">Cerrar sesión</a>
                        </div>
                        <div class="col col1 first">
                            <div class="image round fit">
                                <a href="generic.html" class="link"><img src="assets/images/pic02.jpg" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- Section -->
            <section class="wrapper style1">
                <div class="inner">
                    <header class="align-center">
                        <h2>Aliquam ipsum purus dolor</h2>
                        <p>Cras sagittis turpis sit amet est tempus, sit amet consectetur purus tincidunt.</p>
                    </header>
                    <div class="flex flex-3">
                        <div class="col align-center">
                            <div class="image round fit">
                                <img src="assets/images/pic03.jpg" alt="" />
                            </div>
                            <p>Sed congue elit malesuada nibh, a varius odio vehicula aliquet. Aliquam consequat, nunc quis sollicitudin aliquet. </p>
                            <a href="#" class="button">Learn More</a>
                        </div>
                        <div class="col align-center">
                            <div class="image round fit">
                                <img src="assets/images/pic05.jpg" alt="" />
                            </div>
                            <p>Sed congue elit malesuada nibh, a varius odio vehicula aliquet. Aliquam consequat, nunc quis sollicitudin aliquet. </p>
                            <a href="#" class="button">Learn More</a>
                        </div>
                        <div class="col align-center">
                            <div class="image round fit">
                                <img src="assets/images/pic04.jpg" alt="" />
                            </div>
                            <p>Sed congue elit malesuada nibh, a varius odio vehicula aliquet. Aliquam consequat, nunc quis sollicitudin aliquet. </p>
                            <a href="#" class="button">Learn More</a>
                        </div>
                    </div>
                </div>
            </section>

        </div>

        <!-- Footer -->
        <footer id="footer">
            <div class="copyright">
                <ul class="icons">
                    <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon fa-snapchat"><span class="label">Snapchat</span></a></li>
                </ul>
                <p>&copy; Todos los derechos | 2018 | <a href="http://www.qreative3.com"> | DISEÑADO POR
            </div>
        </footer>
     

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
   <!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: block;">
    <div class="inner">
        Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real 
        Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies.
        <a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a> | 
        <a href="http://politicadecookies.com" target="_blank" class="info">Más información</a>
    </div>
</div>
 
<script>
function getCookie(c_name){
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1){
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1){
        c_value = null;
    }else{
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1){
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}
 
function setCookie(c_name,value,exdays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}
 
if(getCookie('tiendaaviso')!="1"){
    document.getElementById("barraaceptacion").style.display="block";
}
function PonerCookie(){
    setCookie('tiendaaviso','1',365);
    document.getElementById("barraaceptacion").style.display="none";
}
</script>
<!--//FIN BLOQUE COOKIES-->
    </body>
</html>