
<?php

    if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 

    if(isset($_POST['enviar'])){ 

        foreach(@$_POST['cantidad'] as $key => $val) { 
            if($val==0) { 
                unset($_SESSION['carrito'][$key]); 
            }else{ 
                $_SESSION['carrito'][$key]['cantidad']=$val; 
            } 
        }     
    } 
?> 

<!DOCTYPE html> 

<html lang="es">
    <head> 
<meta charset="UTF-8">  


    <link rel="stylesheet" href="css/estilos.css" />  
    <title>Carrito de comidas</title> 
</head> 
<body>
    
    
    
    <div class="fondo2">
	    <form action="#" method="post">
        <div class="form-group">
            <label for="numeroCliente">Cliente</label>
            <?php
                require_once("../includes/connection.php");

                $sql = "SELECT `cliente`.`numeroCliente`, `cliente`.`nombre`, `cliente`.`apellidos`
                              FROM `cliente`";

                $consultaA = mysqli_query($conexion, $sql);

                echo '<select name="cliente" id="">';
                while ($fila = mysqli_fetch_array($consultaA)) {

                    echo '
                                               
                        <option value="' . $fila[numeroCliente] . '">'
                    . $fila[numeroCliente] . ' - ' . $fila[nombre] . ' ' . $fila[apellidos] . '
                                
                       </option>';
                }
                $_SESSION['cliente']=$_POST['cliente'];
                              
             
                echo '</select><br><br>';
		   ?>
		
		
		<label for="numeroEmpleado">Empleado</label>
            <?php
                require_once("../includes/connection.php");

                $sqlA = "SELECT `empleados`.`codigoEmpleado`, `empleados`.`nombre`, `empleados`.`tipoEmpleado`
                              FROM `empleados`";

                $consultaA = mysqli_query($conexion, $sqlA);

                echo '<select name="empleado" id="">';
                while ($fila = mysqli_fetch_array($consultaA)) {

                    echo '
                                               
                        <option value="' . $fila[codigoEmpleado] . '">'
                    . $fila[codigoEmpleado] . ' - ' . $fila[nombre] . ' ' . $fila[tipoEmpleado] . '
                                
                       </option>';
                }
                $_SESSION['empleado']=$_POST['empleado'];
                              
                ?>
                </select><br><br>
                
               
                
                
                <?php
                    echo 'Fecha '.date("d-m-Y H:i:s");   
                    
                ?>
                
                
                   <?php
                foreach($_SESSION['carrito'] as $id => $value) { 
                    echo  $_SESSION['carrito'][$id]['cantidad'];
                } 
                ?>
                
                
	
<h1>PEDIDO COMPRA RESTAURANTE</h1> 

<form method="post" action="index.php?pagina=carrito"> 

    <table  class="tablecenter"> 
        <tr> 
	    <th>ID Comida</th> 
	    
	    <th>Tipo Comida</th> 
	    
            <th>Descripción Comida</th> 
	    
            <th>Cantidad</th> 

            <th>Precio</th> 

            <th>Subtotal</th> 
        </tr> 

                
        <?php 
          require("includes/conexion.php");

            $sql="SELECT * FROM tipocomida WHERE idcomida IN ("; 
                      
                foreach($_SESSION['carrito'] as $id => $value) { 
                    $sql.=$id.","; 
                } 

                $sql=substr($sql, 0, -1).") ORDER BY idcomida ASC"; 

                $query=mysqli_query($conexion, $sql); 

                $total=0; 
                
               $sql = "INSERT INTO venta (fecha, numeroCliente, codigoEmpleado) VALUES(now(), $_SESSION[cliente],$_SESSION[empleado])";
        
		$result = mysqli_query($conexion, $sql);

                while(@$fila=mysqli_fetch_array($query)){ 

                    $subtotal=$_SESSION['carrito'][$fila['idcomida']]['cantidad']*$fila['precio']; 

                    $total+=$subtotal; 
                ?> 

                    <tr> 
			<td><?php echo $fila['idcomida'] ?></td> 
			
			<td><?php echo $fila['tipo'] ?></td> 
			
                        <td><?php echo $fila['descripcion'] ?></td> 
			
                        <td><input type="text" class="numero" name="cantidad[<?php echo $fila['idcomida'] ?>]" maxlength="2" size="2" value="<?php echo $_SESSION['carrito'][$fila['idcomida']]['cantidad'] ?>" /></td> 

                        <td class="numero"><?php echo $fila['precio'] ?> €</td> 

                        <td class="numero"><?php echo number_format($_SESSION['carrito'][$fila['idcomida']]

                        ['cantidad']*$fila['precio'], 2, '.', '') ?> €</td> 
                    </tr> 

                <?php 
                

                $cant=$_SESSION['carrito'][$fila['idcomida']]['cantidad'];
                $idcom=$fila['idcomida'];
                

                
                
                
                $sqlB="SELECT max(numeroVenta) AS numeroVenta FROM venta";   
              $resultB = mysqli_query($conexion, $sqlB);
              $fila= mysqli_fetch_array($resultB);
              $numeroVenta=$fila[0];
               
              
              

	$sqlA = "INSERT INTO `detalleventa`(`cantidadVenta`, `importeVenta`, `numeroVenta`, `codigoComida`) VALUES(".$cant.",". $subtotal.",". $numeroVenta.",". $idcom.")";
        
   
                
              		$resultA = mysqli_query($conexion, $sqlA);
                
                if ($resultA){
                echo 'Introducido correctamente ';
                }else{

                    echo $resultA.'<br>';
                    echo 'Pedido no introducido';
                }
              
                } 
                
                
                $sqlC= "UPDATE `venta` SET `totalVenta`=$total ORDER BY `numeroVenta` DESC LIMIT 1";
                $resultC = mysqli_query($conexion, $sqlC);
                
                

                ?> 

                    
                    
                    
                    
                <tr> 
                    <td colspan="4">Total: <?php echo number_format($total, 2, '.', '') ?> €</td> 
                </tr> 

    </table> 
<div class="centrar"> 

<a href="nueva.php">Ir a ticket</a>

    <button type="submit" name="enviar">Actualizar carrito</button> 
  
<br /> 

<p>PARA QUITAR COMIDA, PONER 0 EN LA CANTIDAD</p>

<!--<a href="index.php?pagina=comidas">Ir a las comidas</a>-->
</div> 
</form> 



 </div>
</body> 
</html>