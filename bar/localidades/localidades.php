

    <script type="text/javascript">
        function mostrarModelos(texto) {
            var peticion;
            
            if (window.XMLHttpRequest) { // Código para IE7+, Firefox, Chrome, Opera, Safari
                
                // El objeto predefinido XMLHttpRequest es el objeto clave de Ajax, ya que nos proporciona las 
                // herramientas para hacer las operaciones y comprobaciones necesarias para la comunicación con el servidor
                
                peticion = new XMLHttpRequest();
            } else { // Código para IE6, IE5
                peticion = new ActiveXObject("Microsoft.XMLHTTP");
            }
            
            // onreadystatechange: Sirve para definir una función que será llamada automáticamente
            // cada vez que cambie la propiedad readyState del objeto
            
            peticion.onreadystatechange = function() {
                
                // Es un condicional asociado a si se ha completado (4) el intercambio de información 
                // y si el servidor ha devuelto un código de respuesta correcta (200)
                
                if (peticion.readyState == 4 && peticion.status == 200) {
                    document.getElementById("poblacion").innerHTML = peticion.responseText;
                }
            }

            peticion.open("GET", "../localidades/poblacion.php?c=" + texto, true);
            peticion.send();
        }
    </script>

        <label>Provincia</label>
        <select name="provincia" onchange="mostrarModelos(this.value)">
            <option value = "">Selecciona una provincia</option>
                <?php include "../localidades/provincia.php" ?>
        </select><br><br>
        <label>Población</label>
        <select name="poblacion" id="poblacion">
            <option value= "">Selecciona una población</option>
        </select>






