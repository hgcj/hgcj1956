<?php 
    session_start(); 

    require("includes/conexion.php"); 

    if(isset($_GET['pagina'])){      
        $paginas=array("comidas", "carrito");      

        if(in_array($_GET['pagina'], $paginas)) {         
            $_pagina=$_GET['pagina'];           
        }else{            
            $_pagina="comidas";             
        }          
    }else{         
        $_pagina="comidas";  
    } 
?>

<!DOCTYPE html> 

<html lang="es">
<meta charset="UTF-8">  

<head> 
    <link rel="stylesheet" href="css/estilos.css" /> 
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>PEDIDOS</title> 
</head> 
 
<body> 
    <div id="container"> 
	<div class="medio">
	    <a href="../privada/entradaprivada.php"><button class="w3-button w3-deep-orange">Ir a zona privada</button></a>
	    <a href="../index.php"><button class="w3-button w3-deep-orange">Ir a HOME</button></a>
	    <a href="index.php"><button class="w3-button w3-deep-orange">Ir a ENTRADA PEDIDO</button></a>
	</div>
        <div id="main"> 
             <?php require($_pagina.".php"); ?>
	</div>
    </div>
    
</body> 
</html>
