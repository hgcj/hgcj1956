<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/estilos.css" />

</head>

<body>
    
    <h3>PEDIDO COMIDAS</h3>
    <a href="../ventas/index.php"><button>Hacer pedidos</button></a>

    <h3>ALTA EMPLEADOS</h3>
    <a href="../privada/register.php"><button>Alta Empleado</button></a>

    <h3>ALTA COMIDAS</h3>
    <a href="formtipocomida.php"><button>Introducir nueva comida</button></a>
    
    <h3>MODIFICAR COMIDAS</h3>
    <a href="formucambiotipocomida.php"><button>Cambiar tipo comida</button></a>
     
    <h3>ELIMINAR COMIDAS</h3>
    <a href="formueliminartipocomida.php"><button>Eliminar tipo comida</button></a>
        
    <h3>ENTRADA BASE DE DATOS</h3>
    <a href="aviso.php"><button>Base de datos</button></a>
        
    <h3>CONSULTAS</h3>
    <a href="../privada.php"><button>Consultas</button></a>
    
    <h3>IR A HOME</h3>
    <a href="logout.php"><button>Salir</button></a>
    




</body>

</html>
