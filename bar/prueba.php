<!DOCTYPE HTML>
<html>
    <head>
        <title>BAR | RESTAURANTE</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="../assets/css/stylec.css">
        <link rel="stylesheet" href="assets/css/main.css" />
	
    </head>
    <body>
        <!-- Header -->
	    <header id="header" class="alt">
		<div class="logo"><a href="index.html">BAR <span>RESTAURANTE</span></a></div>
		<a href="#menu">Menú</a>
	    </header>

	    <!-- Nav -->
	    <nav id="menu">
		<ul class="links">
		    <li><a href="index.php">HOME</a></li>
		    <li><a href="clientes/login.php">ZONA CLIENTES</a></li>
		    <li><a href="empleados/login.php">ZONA EMPLEADOS</a></li>
		    <li><a href="privada/login.php">ZONA PRIVADA</a></li>
		</ul>
	    </nav>
	 <!-- Header -->

      <?php
    require_once("includes/connection.php");



    if (isset($_POST["register"])) {

	if (!empty($_POST['nombreUsuario']) && !empty($_POST['password'] )&& !empty($_POST['tipoEmpleado']) 
	&& !empty($_POST['nombre']) && !empty($_POST['apellidos']) && !empty($_POST['dni']) 
	&& !empty($_POST['direccion']) && !empty($_POST['provincia'])&& !empty($_POST['poblacion']) 
	&& !empty($_POST['sexo']) && !empty($_POST['telefono'])) {

	    $nombreUsuario = $_POST['nombreUsuario'];
	    $password = $_POST['password'];
	    $tipoEmpleado = $_POST['tipoEmpleado'];
       	    $nombre = $_POST['nombre'];
	    $apellidos = $_POST['apellidos'];
	    $dni = $_POST['dni'];
            $direccion = $_POST['direccion'];
	    $idprovincia = $_POST['provincia'];
	    $poblacion = $_POST['poblacion'];
	    $sexo = $_POST['sexo'];
	    $telefono = $_POST['telefono'];
        
        // Sirve para conseguir mostrar la provincia a partir del idprovincia
        // hasta el fin líneas 35 - 39
        $consulta2= "SELECT provincia FROM provincia WHERE idprovincia=" . $idprovincia . ""; 
        
        $resultado2= mysqli_query($conexion,$consulta2) or die (mysqli_error());
        $fila = mysqli_fetch_array($resultado2);	
        $provincia = $fila[0];
	   // ------ Fin de comentario ------

	    $query = mysqli_query($conexion, "SELECT * FROM empleados WHERE nombreUsuario='" . $nombreUsuario . "'");
	    $numrows = mysqli_num_rows($query);
	  
	    if ($numrows == 0) {
		$sql = "INSERT INTO empleados (nombreUsuario, password, tipoEmpleado, nombre, apellidos,dni,direccion, provincia, poblacion,sexo, telefono) 
			VALUES('$nombreUsuario','$password' ,'$tipoEmpleado','$nombre', '$apellidos', '$dni','$direccion', '$provincia', '$poblacion','$sexo','$telefono')";

		$result = mysqli_query($conexion, $sql);
//            echo $sql;

		if ($result) {
		    $message = "Cuenta creada correctamente";
		} else {
		    $message = "Error al ingresar datos de la información!";
		}
	    } else {
		$message = "El nombre de usuario ya existe! Por favor, intenta con otro!";
	    }
	} else {
        
	    $message = "Todos los campos no deben de estar vacios!";
	}
    }
    ?>

    <?php if (!empty($message)) {
	echo "<p class=\"error\">" . "Mensaje: " . $message . "</p>";
    } ?>

    <div class="container mregister ">
	<div id="login">
	    <h1>Registro Empleados</h1>
	    <form name="registerform" id="registerform" action="register.php" method="post">
		<div >
		<p>
		    <label for="user_login">Nombre Usuario <br />
                        <input type="text" name="nombreUsuario" id="nombreUsuario" class="input" size="32" value=""  /></label>
		</p>

		<p>
		    <label for="user_pass">Contraseña<br />
                        <input type="password" name="password" id="password" class="input" value="" size="32" /></label>
		</p>
                   
		<label>Tipo Cliente</label>
		    <select name="tipoEmpleado"> 
			<option value = "Encargado">Encargado</option>
			<option value = "Barra">Barra</option>
			<option value = "Sala">Sala</option>
			<option value = "Cocina">Cocina</option>
			<option value = "Limpieza">Limpieza</option>
		    </select> 
		
		<p>
		    <label for="user_pass">Nombre<br />
                        <input type="text" name="nombre" id="nombre" class="input" value="" size="32" /></label>
		</p>

		<p>
		    <label for="user_pass">Apellidos<br />
                        <input type="text" name="apellidos" id="apellidos" class="input" value="" size="20" /></label>
		</p>

		<p>
		    <label for="user_pass">DNI<br />
                        <input type="text" name="dni" id="dni" class="input" value="" size="20" /></label>
		</p>
            
		<p>
		    <label for="user_pass">Dirección<br />
                        <input type="text" name="direccion" id="direccion" class="input" value="" size="20" /></label>
		</p>
                
                <?php
                    include "../localidades/localidades.php";
                ?>
		
		<br><br><label>Sexo</label>
		<select name="sexo"> 
		    <option value = "Hombre">Hombre</option>
		    <option value = "Mujer">Mujer</option>
		</select>
                
		<p>
		    <label for="user_pass">Teléfono<br />
                        <input type="text" name="telefono" id="telefono" class="input" value="" size="20" /></label>
		</p>
		
		<p class="submit">
		    <input type="submit" name="register" id="register" class="button" value="Registrar" />
		</p>
                                
		</div>
	    </form>
          
	</div>
	  <p>CERRAR LA SESIÓN ANTES DE SALIR<a href="logout.php" class="button"> Cerrar sesión</a>
    </div>
     
	
        <!-- Scripts -->
	    <script src="assets/js/jquery.min.js"></script>
	    <script src="assets/js/jquery.scrolly.min.js"></script>
	    <script src="assets/js/jquery.scrollex.min.js"></script>
	    <script src="assets/js/skel.min.js"></script>
	    <script src="assets/js/util.js"></script>
	    <script src="assets/js/main.js"></script>
	<!-- Scripts -->
    </body>
</html>
