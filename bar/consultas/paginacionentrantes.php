<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="css/estilos.css"/>
<head>
    <meta charset="UTF-8">
    <title>BAR | RESTAURANTE</title>
</head>
<?php
    require_once("../includes/connection.php"); 

    // Pone una cookie durante 1 hora al cliente seleccionado del formulario
    if (isset($_POST["selectCliente"])) {
        setcookie("clienteSeleccionado", $_POST["selectCliente"], time()+3600);
        
        // Refresca la página y activa la cookie
        header("Location:paginaciontipocomidas.php");
    }

$url = "http://localhost/bar140218/consultas/paginacionentrantes.php";

$consulta1 = "SELECT `tipocomida`.`idcomida`, `tipocomida`.`tipo`, `tipocomida`.`descripcion`, `tipocomida`.`precio`
FROM `tipocomida` WHERE (`tipocomida`.`tipo` ='Entrantes')";

$rs = mysqli_query($conexion, $consulta1);
$numeroRegistros = mysqli_num_rows($rs);

// Si hay registros
if ($numeroRegistros > 0) {


    // Limita la busqueda
    $TAMANO_PAGINA = 10;
    $pagina = false;
    

    // Examina la pagina a mostrar y el inicio del registro a mostrar
    if (isset($_GET["pagina"])) {
        $pagina = $_GET["pagina"];
    }
   
    
    if (!$pagina) {
        $inicio = 0;
        $pagina = 1;
    } else {
        $inicio = ($pagina - 1) * $TAMANO_PAGINA;
    }
    // Calcula el total de paginas
    $totalPaginas = ceil($numeroRegistros / $TAMANO_PAGINA);

    echo '<h2>ENTRANTES</h2>';

    // Pone el número de registros total, el tamaño de página y la página que se muestra
    echo '<h3>Número de registros: ' . $numeroRegistros . '</h3>';
    echo '<h3>Mostrando la página ' . $pagina . ' de ' . $totalPaginas . ' páginas</h3>';

    $consulta = $consulta1 . " LIMIT " . $inicio . "," . $TAMANO_PAGINA;
    $rs = mysqli_query($conexion, $consulta);

    // Crea la tabla de resultados
    echo '<table><tr>'
    . '<td class="tabla"><strong>ID COMIDA</strong></td>'
    . '<td class="tabla"><strong>TIPO</strong></td>'
    . '<td class="tabla"><strong>DESCRIPCIÓN</strong></td>'
    . '<td class="tabla"><strong>PRECIO</strong></td>'
    . '</tr>';

    // Muestra cada fila de resultados
    while ($fila = mysqli_fetch_array($rs)) {
        echo '<tr>'
	. '<td>', $fila['idcomida'], '</td>'
        . '<td>', $fila['tipo'], '</td>'
	. '<td>', $fila['descripcion'], '</td>'
	. '<td id="precio">', $fila['precio'] ,' € </td>'
	. '</tr>';
    }
    echo '</table><br>';

    // Código para paginar los resultados
    if ($totalPaginas > 1) {
        if ($pagina != 1) {
            echo '<a href="' . $url . '?pagina=' . ($pagina - 1) . '"><img src="images/izq.gif" border="0"></a>';
        }
        for ($i = 1; $i <= $totalPaginas; $i++) {
            if ($pagina == $i) {
                // Si muestra el índice de la página actual, no coloca enlace
                echo $pagina;
            } else {
                // Si el índice no corresponde con la página mostrada actualmente,
                // se coloca el enlace para ir a esa página
                echo '  <a href="' . $url . '?pagina=' . $i .'">' . $i . '</a>  ';
            }
        }
        if ($pagina != $totalPaginas) {
            echo '<a href="' . $url . '?pagina=' . ($pagina + 1) . '"><img src="images/der.gif" border="0"></a>';
        }
    }
}
